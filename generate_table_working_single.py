import os
from datetime import datetime
from math import log10
from os import listdir
from os.path import isfile, join
from subprocess import PIPE, Popen, STDOUT
from statistics import median


from constants_single import *


def main():
    global PLANNERS
    get_planners()
    get_domains()

    init_results_dict()
    init_table_dict()
    init_total_dict()
    init_p_star_dict()

    preprocess()
    get_instances_dict()

    for p in PLANNERS:
        print 'Genero: {}'.format(p)
        path_p = join(PREPROCESS, p)
        p_results = read_file(path_p)

        get_results_indx(p_results)
        populate_results_dict(p_results, p)

    populate_table_dict()

    ''' output '''
    output_folder = str(datetime.now())
    output_path = OUTPUT_PATH.format(output_folder)
    generate_latek_files(output_path)

    # NOPE
    # generate_coverage_plot_data(output_path)


def generate_coverage_plot_data(output_path):
    coverage_plot_dir = join(output_path, 'coverage_plot')

    os.makedirs(coverage_plot_dir)
    initialize_dict_solved_by_time()

    ''' TODO: fare dizionario problemi e controllare input '''

    ''' conto '''
    for p in PLANNERS:
        for d in DOMAINS:
            for i in RESULTS_DICT[p][d].keys():
                increase_solved_by_time(p, d, i)


    ''' calcolo percentuale '''
    for p in PLANNERS:
        for i in xrange(0, 300 / GRANULARITY + 1):
            percentage_solved_by_time(p, GRANULARITY * i)

    ''' SCRIVO I DATI '''
    write_solved_by_time(coverage_plot_dir)
    write_gnuplot_script(coverage_plot_dir)
    get_stdout("cp \"{}\" \"{}\"".format(COVERAGE_DOCUMENT, coverage_plot_dir))
    os.chdir(coverage_plot_dir)
    get_stdout("gnuplot coverage.gpl")
    print "Comando: pdflatex {}".format(COVERAGE_DOCUMENT)
    get_stdout("pdflatex {}".format(COVERAGE_DOCUMENT))
    get_stdout("pdflatex {}".format(COVERAGE_DOCUMENT))
    os.chdir(CURRENT_FOLDER)


def write_gnuplot_script(coverage_plot_dir):
    gplot_script_path = join(coverage_plot_dir, 'coverage.gpl')
    gplot_template = read_file(GNUPLOT_TEMPLATE)

    app = '\nplot '
    for indx_p, p in enumerate(PLANNERS, start=2):
        app += "\"coverage.dat\" using 1:{} title \"{}\" with lines,".format(
            str(indx_p), PLOT_DICT[p])
    app = app[0:-1]
    gplot_template += app

    write_file(gplot_template, gplot_script_path)


def percentage_solved_by_time(p, var_t):
    num = float(SOLVED_BY_TIME[p][TOTAL][var_t])
    den = float(get_total_instances())
    var_t_per = round(num / den * 100, 2)
    SOLVED_BY_TIME[p][TOTAL][var_t] = var_t_per
    pass


def write_solved_by_time(coverage_plot_dir):
    output_path = join(coverage_plot_dir, 'coverage.dat')
    txt = ''
    for t in xrange(0, 300 / GRANULARITY + 1):
        var_t = t * GRANULARITY
        txt += str(var_t)
        for p in PLANNERS:
            txt += ', {}'.format(SOLVED_BY_TIME[p][TOTAL][var_t])
            txt = txt[0:-1]
        txt += '\n'

    write_file(txt, output_path)


def initialize_dict_solved_by_time():
    for p in PLANNERS:
        SOLVED_BY_TIME[p] = {
        }
        for d in DOMAINS:
            SOLVED_BY_TIME[p][d] = {}

            for t in xrange(0, 300 / GRANULARITY + 1):
                SOLVED_BY_TIME[p][d][t * GRANULARITY] = 0
        SOLVED_BY_TIME[p][TOTAL] = {}
        for t in xrange(0, 300 / GRANULARITY + 1):
            SOLVED_BY_TIME[p][TOTAL][t * GRANULARITY] = 0


def increase_solved_by_time(p, d, i):
    # print RESULTS_DICT[p][d][i].keys()
    '''
    results_pdi:
        - aggregated_run
        - 1, 2, 3, 4, 5, ...,
        - median_run
    '''
    results_pdi = RESULTS_DICT[p][d][i]

    runs_key = results_pdi[RUNS].keys()
    times = [results_pdi[RUNS][k][TIME] for k in runs_key]
    times = [float(t) for t in times if 'NA' != t]
    # print results_pdi[OTHERS]['median_run'][TIME]
    if len(times) == 0:
        m_dpi = 300
    else:
        m_dpi = median(times)

    for i_t in xrange(0, 300 / GRANULARITY + 1):
        time = i_t * GRANULARITY
        tf = float(time)

        if m_dpi < tf:
            SOLVED_BY_TIME[p][d][time] += 1
            SOLVED_BY_TIME[p][TOTAL][time] += 1


def preprocess():
    for p in PLANNERS:
        preprocess_p(p)


def preprocess_p(p):

    p_str = read_file_p(p)
    rows = split_p_entries(p_str)
    keys = rows[0].split(',')
    first_row = rows[0]

    new_p_str = ''

    # Tolgo la prima linea
    rows = rows[1:]
    for r in rows:
        r_dict = build_row_dict(r, keys)
        assert(r_dict['Planner'] == p)
        r_time = r_dict[TIME].replace('\t', '').replace('\r', '').replace(' ', '')
        if 'NA' in r_dict[TIME] or r_time == '':
            r_dict[TIME] = '300.0'

        r_new = from_row_dict_to_row(r_dict, keys)
        new_p_str += '{}\n'.format(r_new)

    new_p_str = first_row + '\n' + new_p_str

    with open(join(PREPROCESS, p), 'w') as fout:
        fout.write(new_p_str)


def from_row_dict_to_row(row_dict, keys):
    row_str = ''
    for i_k, k in enumerate(keys, start=0):
        row_str += '{},'.format(row_dict[k])
    return row_str[0:-1]


def keep_row(r_dict, p):
    run = int(r_dict['run'])
    if p == 'LPG_MA_BFWS_20180919_A2_F0_unovelty_FN_img_exchpred':
        if run != 5:
            return False

    if p == 'LPG_MA_BFWS_20180919_A2_F20_unovelty_FN_img_exchpred':
        if run != 1:
            return False

    return True


def build_row_dict(r, keys):
    r = r.replace(' ', '')
    data = r.split(',')
    r_dict = {}
    for i, k in enumerate(keys, start=0):
        r_dict[k] = data[i]
    return r_dict


def split_p_entries(string_p):
    splitted_p_str = string_p.split('\n')
    results = []
    for r in splitted_p_str:
        if r == '':
            continue
        results.append(r)
    return results


def read_file_p(p):
    path_p = join(RISULTATI, p)
    p_results = read_file(path_p)
    return p_results


def get_planners():
    global PLANNERS

    # PRENDO DALLA CARTELLA ''RISULTATI''
    PLANNERS = list_files(RISULTATI)
    PLANNERS = [p for p in PLANNERS if '.py' not in p]

    PLANNERS = [
        'E1_20181112_BFWS_Single_A1_F4_FN_img_exchpred',
        'E1_20181112_BFWS_Single_A2_F4_FN_img_exchpred', 
        # 'E1_20181112_BFWS_Single_A2_F4_FN_img_exchpred_nopruning', 
        'E1_20181112_BFWS_Single_A1_F8_FN_img_exchpred',
        'E1_20181112_BFWS_Single_A2_F8_FN_img_exchpred',
        # 'E1_20181112_BFWS_Single_A2_F8_FN_img_exchpred_nopruning',
        # 'E1_20181112_BFWS_Single_A1_F7_FN_img_exchpred',
        # 'E1_20181112_BFWS_Single_A2_F7_FN_img_exchpred',
        # 'E1_20181112_BFWS_Single_A2_F7_FN_img_exchpred_nopruning'
    ]

def generate_latek_files(output_path):
    ''' tabelle ipc_t, solved '''
    latex_data = [SOLVED_PROBLEMS, IPC_T]
    os.makedirs(output_path)
    for metric in latex_data:
        # generate_metric_latex(metric, output_path)
        metric_table = generate_latek_table(metric)
        generate_generic_latex(metric_table, metric, output_path)

    ''' tabella single '''
    single_table = generate_single_table()
    generate_generic_latex(single_table, 'single_coverage', output_path)


def generate_single_body_table():
    s_domains = list(DOMAINS)
    s_domains.sort()

    first_row = '\\hline \n'
    first_row += 'Domain & I'
    for p in PLANNERS:
        first_row += ' & {}'.format(VARIANT_DICT[p])
    first_row += '\\\\' + HLINE

    body = first_row
    for d in s_domains:
        row = '{}&'.format(LATEX_DOMAIN[d])
        row += get_number_of_instances(d)
        for p in PLANNERS:
            row += '&{}'.format(get_per_coverage_dp(d, p))
        row += '\\\\ \n'

        body += row
    body += '\\hline \n'

    body += total_single_row()
    return body


def total_single_row():
    row = 'Summary &'
    row += str(get_total_instances())
    for p in PLANNERS:
        row += '& {}'.format(get_per_coverage_p(p))
    row += '\\\\ \\hline \n'
    return row


def get_per_coverage_p(p):
    num = TOTAL_DICT[p][SOLVED_PROBLEMS]
    den = get_total_instances()
    tot_cov = float(num) / float(den)
    assert tot_cov <= 1.0
    return '{}\\%'.format(str(round(tot_cov * 100, 2)))


def get_coverage_dp(d, p):
    return str(TABLE_DICT[d][p][METRICS][SOLVED_PROBLEMS])


def get_per_coverage_dp(d, p):
    num = float(get_coverage_dp(d, p))
    den = float(get_number_of_instances(d))
    coverage_per = num / den
    assert coverage_per <= 1.0
    return '{}\\%'.format(str(round(coverage_per * 100, 2)))


def get_number_of_instances(d):
    return str(INSTANCES_DICT[d])


def generate_single_table():
    body_table = generate_single_body_table()
    # num_col = 2 + len(PLANNERS)
    tek_table = TABLE_TEMPLATE.replace('#BODY#', body_table)
    column_str = 'cl' + 'c' * len(PLANNERS)
    tek_table = tek_table.replace('#COL#', column_str)
    tek_table = tek_table.replace('#CAPTION#', '')
    return tek_table


# def generate_metric_latex(metric, output_path):
    # tex_body = generate_latek(metric)
    # tex_document = LATEX_TEMPLATE.replace('#BODY#', tex_body)
    # latex_name = '{}.tex'.format(metric)
    # metric_output = join(output_path, latex_name)
    # write_file(tex_document, metric_output)
    # latex_command = 'pdflatex {}'.format(latex_name)
    # print 'Comando: {}'.format(latex_command)

    ''' compilazione '''
    # os.chdir(output_path)
    # get_stdout(latex_command)
    # get_stdout(latex_command)
    # os.chdir(CURRENT_FOLDER)


def generate_generic_latex(tex_body, name, output_path):
    tex_document = LATEX_TEMPLATE.replace('#BODY#', tex_body)
    latex_name = '{}.tex'.format(name)
    metric_output = join(output_path, latex_name)
    write_file(tex_document, metric_output)

    latex_command = 'pdflatex {}'.format(latex_name)
    print 'Comando: {}'.format(latex_command)
    os.chdir(output_path)
    get_stdout(latex_command)
    get_stdout(latex_command)
    os.chdir(CURRENT_FOLDER)


def generate_latek_table(metric):
    assert(metric in [IPC_T, SOLVED_PROBLEMS])

    body_table = generate_body_table(metric)
    num_col = 1 + len(PLANNERS)

    tek_table = TABLE_TEMPLATE.replace('#BODY#', body_table)
    column_str = '|' + 'c|' * num_col
    tek_table = tek_table.replace('#COL#', column_str)
    tek_table = tek_table.replace('#CAPTION#', TABLE_CAPTION)
    return tek_table


def from_metric_2_cap(metric):
    splitted_metric = metric.split('_')
    new_metric = [ms[0].upper() + ms[1:] for ms in splitted_metric]
    return ' '.join(new_metric)


def write_file(txt_file, output_path):
    with open(output_path, 'w') as fout:
        fout.write(txt_file)


def get_stdout(eval_command):
    p = Popen(
        eval_command, shell=True, stdin=PIPE,
        stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    return output


def get_separated_domains():
    '''
    Modificare qui per cambiare layout domini nella tabella
    '''
    codmap_d = [d for d in DOMAINS if d not in CODMAP_DOMAINS]
    codmap_d.sort()
    others_domains = [d for d in DOMAINS if d not in CODMAP_DOMAINS]
    others_domains.sort()
    return codmap_d, others_domains
    # sorted_domains = list(DOMAINS)
    # sorted_domains.sort()


def generate_body_table(metric):
    table_body = generate_first_row()

    first_domains, second_domains = get_separated_domains()

    from_codmap = '{\\bf From CoDMAP}' + '&' * len(PLANNERS) + '\\\\\n'
    table_body += from_codmap
    for d in first_domains:
        table_body += generate_tek_row(d, metric)
    table_body += HLINE

    from_privacy = '{\\bf From TODO}' + '&' * len(PLANNERS) + '\\\\\n'
    table_body += from_privacy
    for d in second_domains:
        table_body += generate_tek_row(d, metric)
    table_body += HLINE

    for total_metric in TABLE_FIELDS_LATEX:
        table_body += generate_total_row(total_metric)
    table_body += HLINE

    return table_body


def generate_first_row():
    first_row = '\\hline\nDomain '
    for p in PLANNERS:
        simplified_p = VARIANT_DICT[p]
        first_row += '& {} \n'.format(simplified_p)
    first_row = first_row[0:-1]
    first_row += '\\\\ \\hline \n'

    return first_row


def generate_total_row(metric):
    total_row = total_name_field(metric)
    for p in PLANNERS:
        string_data = str(TOTAL_DICT[p][metric])
        total_row += '{}&'.format(string_data)
    total_row = total_row[0:-1] + ' \\\\ \n'
    return total_row


def iter_problem_d_p(d, p):
    return RESULTS_DICT[p][d].iteritems()


def check_and_replicate_runs(pd_runs):
    '''
    controllo che il numero di run sia uguale al numero di
    righe riferite ad un problema ed ad un pianificatore

    Alternative:
        1) Replico i dati mancanti
        2) Ignoro ed uso la mediana per i planner che hanno multirun
    '''

    if REPLICATE is True:
        num_of_run = len(pd_runs.keys())
        if num_of_run != EXPECTED_RUN:
            assert(num_of_run == 1)
            to_replicate = pd_runs['1']
            for indx in xrange(1, EXPECTED_RUN):
                pd_runs[str(indx + 1)] = to_replicate
        assert(len(pd_runs.keys()) == EXPECTED_RUN)
        return pd_runs
    else:
        return pd_runs


def extend_results_with_run():
    for d in DOMAINS:
        for p in PLANNERS:

            for problem_name, pd_runs in iter_problem_d_p(d, p):

                ''' se voglio replicare i dati '''
                # pd_runs = check_and_replicate_runs(pd_runs)

                aggregated_dict = {}

                # Problemi risolti
                aggregated_dict['Solved'] = aggregate_solved_problem(pd_runs)

                # Tempo
                aggregated_dict[TIME] = aggregate_times(pd_runs)

                for k_data in ['Received', 'Msgs', 'States', 'Quality']:
                    aggregated_dict[k_data] = aggregate_metric(k_data, pd_runs)

                ''' solved problems '''

                median_dict = {}

                median_data = ['Received', 'Msgs', 'States', 'Quality', TIME]
                for k_data in median_data:
                    median_dict[k_data] = get_median(k_data, pd_runs)

                pd_runs[OTHERS]['aggregated_run'] = aggregated_dict
                pd_runs[OTHERS]['median_run'] = median_dict


def get_median(k_data, pd_runs):

    metric_quantities = []
    for indx, indx_run in pd_runs[RUNS].iteritems():
        metric_quantities.append(indx_run[k_data])
    metric_quantities = clean_quantities(metric_quantities)
    metric_quantities.sort()
    n = len(metric_quantities)

    odd = n % 2 == 0

    if n == 0:
        return 'NA'
    elif n == 1:
        return metric_quantities[0]
    elif odd is True:  # pari
        indx_1 = n / 2 - 1
        indx_2 = n / 2
        median = (metric_quantities[indx_1] + metric_quantities[indx_2]) / 2
    else:  # dispari
        median = metric_quantities[n / 2]
    return median


def total_name_field(metric):
    if metric == SOLVED_PROBLEMS:
        # return 'TOTAL ({})&'.format(str(INSTANCES_DICT[TOTAL]))
        return '\\bf{{Overall}} ({})&'.format(str(get_total_instances()))
    elif metric == IPC_T:
        return 'IPC(T)&'
    elif metric == IPC_Q:
        return 'IPC(Q)&'
    elif metric == TIME:
        return 'Time&'
    elif metric == QUALITY:
        return 'Quality&'
    elif metric == MESSAGES:
        return 'kMessages&'
    elif metric == RECEIVED:
        return 'kReceived&'
    elif metric == EXPANDED_STATES:
        return 'kStates&'


def generate_tek_row(d, metric):
    row = '\t'
    row += '{}&'.format(LATEX_DOMAIN[d])
    for p in PLANNERS:
        data = get_metric_d_p(d, p, metric)
        row += '{}&'.format(data)

    row = row[0:-1] + ' \\\\ \n'
    return row


def get_metric_d_p(d, p, metric):
    return str(
        TABLE_DICT[d][p][METRICS][metric])


def populate_table_dict():
    # get_type_of_compare()

    extend_results_with_run()

    for d in DOMAINS:
        for p in PLANNERS:
            for metric_k in TABLE_FIELD:
                TABLE_DICT[d][p][METRICS][metric_k] = \
                    get_aggregated_metric(d, p, metric_k)

    populate_p_star()

    ''' calcolo i totali '''
    for p in PLANNERS:
        TOTAL_DICT[p][IPC_T] = get_total_p_metric(p, IPC_T)
        TOTAL_DICT[p][IPC_Q] = get_total_p_metric(p, IPC_Q)

        for metric in TABLE_FIELD:
            TOTAL_DICT[p][metric] = \
                get_total_p_metric(p, metric)


def aggregate_times(pd_runs):
    ''' controlli da riattivare '''
    num_of_run = len(pd_runs.keys())

    times_list = set([])
    for indx_run, run in pd_runs[RUNS].iteritems():
        if run[TIME] == 'NA':
            times_list.add(300.0)
        else:
            times_list.add(float(run[TIME]))

    return str(float(sum(times_list)) / float(num_of_run))


def aggregate_solved_problem(pd_runs):
    '''
    Un problema e' risolto se gli indici Time
    e Quality non sono NA
    '''
    solved = 0
    # num_of_run = len(pd_runs.keys())

    '''
    Controllo da abilitare.
    Disabilitato per il Paper
    '''
    # assert num_of_run == EXPECTED_RUN

    # '''
    # Controllo se l'istanza per il pianificatore
    # con un solo run e' risolta
    # '''
    # if num_of_run == 1 and num_of_run != EXPECTED:

    for indx_run, run in pd_runs[RUNS].iteritems():
        if check_solved_run(run):
            solved += 1
        # if 'NA' in run[TIME] and 'NA' in run['Quality']:
        #     pass
        # elif 'NA' not in run[TIME] and 'NA' in run['Quality']:
        #     pass
        # else:
        #     solved += 1
    return str(solved)


def check_solved_run(run):
    # print run[TIME]
    # print run['Quality']
    # if 'NA' in run[TIME] and 'NA' in run['Quality']:
    #     return False
    # # if 'NA' in run[TIME] and '' in run['Quality']:  # eccezione da togliere
    #     # return True
    # elif 'NA' not in run[TIME] and 'NA' in run['Quality']:
    #     return False
    rq = run['Quality']
    rq = clean_str(rq)
    if 'NA' in rq:
        return False
    else:
        return True


def aggregate_metric(k_data, pd_runs):
    '''
    da riabilitare
    '''
    # assert len(pd_runs.keys()) == EXPECTED_RUN
    times_list = set([])
    for indx_run, run in pd_runs[RUNS].iteritems():

        if run[k_data] == 'NA':
            pass
        else:
            times_list.add(float(run[k_data]))

    if len(times_list) == 0:
        return 'NA'

    return str(sum(times_list) / len(times_list))


def populate_p_star():
    '''calculate P* for each domains'''
    for d in DOMAINS:
        populate_p_star_per_d(d)

    for d in DOMAINS:
        for p in PLANNERS:
            TABLE_DICT[d][p][METRICS][IPC_T] = \
                get_aggregated_metric(d, p, IPC_T)
            TABLE_DICT[d][p][METRICS][IPC_Q] = \
                get_aggregated_metric(d, p, IPC_Q)


# def get_type_of_compare():
#     global MEDIAN_COMPARE
#     '''
#     Funzione che serve a capire come calcolare l'IPC score per Q e T
#     a seconda del numero di run istanziati dai diversi pianificatori.

#     se run > 1 -> mediana

#     DISABILITATO
#     '''
#     # instances_run = set([])
#     # for indx_p, p_dict in RESULTS_DICT.iteritems():
#     #     for indx_d, pd_dict in p_dict.iteritems():
#     #         for indx_instance, instance in pd_dict.iteritems():
#     #             instance_run = len(get_just_key_run(instance.keys()))
#     #             instances_run.add(instance_run)
#     # len_instance_run = len(instances_run)

#     '''
#     Questi cicli aggiungono il RUN ad un set.
#     Es output set([1,2]) -> 2 run
#     '''

#     # assert MEDIAN_COMPARE is None

#     # if len_instance_run > 1:
#     #     MEDIAN_COMPARE = True

#     # elif len_instance_run == 1:
#     #     MEDIAN_COMPARE = False

#     MEDIAN_COMPARE = True


def populate_p_star_per_d(d):
    instances = RESULTS_DICT[PLANNERS[0]][d].keys()

    for pname in instances:
        P_STAR_DICT[d][T][pname] = {T_STAR: float('inf'), P_STAR: None}
        P_STAR_DICT[d][Q][pname] = {Q_STAR: float('inf'), P_STAR: None}

        for p in PLANNERS:
            instance_p_infos = RESULTS_DICT[p][d][pname]

            instance_p_infos = instance_p_infos[RUNS]

            for indx_run, instance_run_data in instance_p_infos.iteritems():
                t_p = convert_data_for_compare(instance_run_data, TIME)
                q_p = convert_data_for_compare(instance_run_data, QUALITY)

                if t_p <= get_t_or_q_star(d, pname, T):
                    set_t_p_star(d, pname, t_p, p)

                if q_p <= get_t_or_q_star(d, pname, Q):
                    set_q_p_star(d, pname, q_p, p)


def convert_data_for_compare(instance, metric):
    return str_to_float(instance[metric])


def str_to_float(float_number):
    try:
        return float(float_number)
    except:
        return 'NA'


def get_t_or_q_star(d, pname, t_or_q):

    return P_STAR_DICT[d][t_or_q][pname]['{}*'.format(t_or_q)]


def set_t_p_star(d, pname, t_star, p_star):

    P_STAR_DICT[d][T][pname][T_STAR] = t_star
    P_STAR_DICT[d][T][pname][P_STAR] = p_star


def set_q_p_star(d, pname, t_star, p_star):
    P_STAR_DICT[d][Q][pname][Q_STAR] = t_star
    P_STAR_DICT[d][Q][pname][P_STAR] = p_star


def get_aggregated_metric(d, p, metric):
    if metric == SOLVED_PROBLEMS:
        return get_aggregated_solved_problems(d, p)
    elif metric in [IPC_T, IPC_Q]:
        t_or_q = T if metric == IPC_T else Q
        return get_aggregated_ipc(d, p, t_or_q)
    elif metric in AVG_FIELDS:
        return avg_quantity(d, p, metric)


def get_total_p_metric(p, metric):
    # PROBLEMI RISOLTI
    # ['solved_problems']
    if metric == SOLVED_PROBLEMS:
        return get_total_p_solved_problems(p)

    elif metric in [IPC_T, IPC_Q]:
        return get_total_ipc(p, metric)

    # VERSIONE DI BAZZOTTI
    # elif metric in [IPC_T, IPC_Q]:
    #     return get_total_ipc_bazzotti(p, metric)

    # CAMPI DA MEDIARE
    # [TIME, 'Quality', 'Msgs', 'Received', 'States']
    elif metric in AVG_FIELDS:
        return avg_total_quantity(p, metric)


def get_total_ipc(p, x):
    quantities = []
    for d in DOMAINS:
        ipc_d_t = TABLE_DICT[d][p][METRICS][x]
        if ipc_d_t != 'NA':
            quantities.append(ipc_d_t)
    return sum(quantities)


# def get_total_ipc_bazzotti(p, x):
    
#     if x == IPC_T: 
#         totale=0
#         tempomax = 300
#         tempomax2=tempomax-1
#         punti_1=0.0
#         punti_2=0.0
#         tempomin=1.0
#         risolti1=0.0
#         risolti2=0.0

#         totale=0
#         elenco1=[]
#         elenco2=[]
#         tempi1=0.0
#         tempi2=0.0
#         i=-1
        
#         for d in DOMAINS:
            
#             instances = RESULTS_DICT[PLANNERS[0]][d].keys()
#             for pname in instances:
                
#                 primo = RESULTS_DICT[p][d][pname][TIME]
#                 secondo = P_STAR_DICT[d][T][pname][T_STAR]
#                 # print 'SECONDO'
#                 # print secondo
#                 # print P_STAR_DICT[d][T][pname].keys()
#                 # exit(1) 
#                 # print("PRIMO" + primo)
#                 # print("SECONDO" +str(secondo))
#             if secondo == 0:
#                 secondo=300

#             if primo.replace(" ","") == "NA":
#                 primo="300"


#             if float(primo) == 0 :
#                 primo=str(tempomin)
    

#             if float(secondo) == 0 :
#                 secondo=str(tempomin)
    


#     #elenco1.append(float(primo[3]))
#     #elenco2.append(float(secondo[3]))
#             totale=totale+1
#     #elenco1.append(float(primo[3]))
#     #elenco2.append(float(secondo[3]))
#     #print "t= "+str(totale)
#             if float(primo) > tempomax2 and float(secondo) > tempomax2:
#                 elenco1.append(tempomax)
#                 elenco2.append(tempomax)
#                 tempi1=tempi1+tempomax
#                 tempi2=tempi2+tempomax
#                 continue
#             if float(primo) > tempomax2:
#                     punti_2=punti_2+1
#                 risolti2=risolti2+1
#                 elenco1.append(tempomax)
#                 elenco2.append(float(secondo))
#                 tempi1=tempi1+tempomax
#                 tempi2=tempi2+float(secondo)
#                 continue
#             if float(secondo) > tempomax2:
#                 punti_1=punti_1+1
#                 risolti1=risolti1+1
#                 elenco1.append(float(primo))
#                 elenco2.append(tempomax)
#                 tempi1=tempi1+float(primo)
#                 tempi2=tempi2+tempomax
#                 continue
#             elenco1.append(float(primo))
#             elenco2.append(float(secondo))
#             tempi1=tempi1+float(primo)
#             tempi2=tempi2+float(secondo)
#             if float(primo) > float(secondo):
#                     punti_2=punti_2+1
#                     if float(secondo) == -1:
#                         punti_1=punti_1+0.01
#                 else:
#                     punti_1=punti_1+float(1.0/(1.0+log10(float(primo)/float(secondo))))
#             else:
#                 punti_1=punti_1+1
#                 if float(primo) == -1 and float(secondo) !=  float(primo):
#                     punti_2=punti_2+0.01
#                 else:
#                     punti_2=punti_2+float(1/(1+log10(float(secondo)/float(primo))))
    

#             risolti2=risolti2+1
#             risolti1=risolti1+1

#         return punti_1

#     if x == IPC_Q:
        
#         punti_1=0.0
#         punti_2=0.0

#         risolti1=0.0
#         risolti2=0.0

#         totale=0
#         elenco1=[]
#         elenco2=[]
#         tempi1=0.0
#         tempi2=0.0
#         i=-1
        
#         for d in DOMAINS:
#             instances = RESULTS_DICT[PLANNERS[0]][d].keys()
#         for pname in instances:
                
#                 primos = RESULTS_DICT[p][d][pname]['Quality']
        
#             secondos = P_STAR_DICT[d][Q][pname][Q_STAR]

#             if secondos == 0:
#                 secondos="-1"

#             if primos.replace(" ","") == "NA":
#                 primos="-1"


#             primo=float(primos)
#             secondo=float(secondos)
#             if float(0) >= 1 and  float(0) < 200 :
#                 if  float(primos) > 0 or float(secondos)>0 :
#                     if float(primos) != -1:
#                         primo=float(primos)* -1.0
#                         if float(secondos) != -1:   
#                             secondo=float(secondos)* -1.0
#             totale=totale+1

#             if float(primo) == -1 and float(secondo) == -1:
#                 continue
#             if float(primo) == -1:
#                 punti_2=punti_2+1
#                 risolti2=risolti2+1
#                 continue
#             if float(secondo) == -1:
#                 punti_1=punti_1+1
#                 risolti1=risolti1+1
#                 continue
#             if float(primos) == 0  and float(secondo) == 0:
#                 punti_1=punti_1+1
#                 punti_2=punti_2+1
#                 risolti1=risolti1+1
#                 risolti2=risolti2+1
#                 continue
#             if float(primos) == 0:      
#                 punti_2=punti_2+1
#                 risolti2=risolti2+1
#                 continue
#             if float(secondo) == 0:
#                 punti_1=punti_1+1
#                 risolti1=risolti1+1
#                 continue
#             if float(primo) > float(secondo):
#                     punti_2=punti_2+1
#                 punti_1=punti_1+float(float(secondo)/float(primo))
#             else:
#                 punti_1=punti_1+1
#                 punti_2=punti_2+float(float(primo)/float(secondo))
#             risolti2=risolti2+1
#             risolti1=risolti1+1
#         return punti_1


# SINGLE RUN
# def avg_quantity(d, p, metric):
#     p_d_subdict = RESULTS_DICT[p][d]
#     #print p_d_subdict
#     #print metric 
#     quantities = [
#         p_d_subdict[instance_k][metric] for instance_k in p_d_subdict.keys()]

#     if metric == 'Quality':
#         print 'DOMINIO: {}'.format(d)
#         print 'PLANNER: {}'.format(p)
#         # print quantities

#     quantities = clean_quantities(quantities)
#     if len(quantities) != 0:
#         ratio = sum(quantities) / len(quantities)

#         if metric == 'Quality':
#             print '{}'.format(ratio)
#             print

#         return format_data_table(ratio, metric)
#     else:
#         return 'NA'
# MULTIRUN


def solved_by_all(instance, d):
    ''' se si vuole mediare su tutto indipendente dalle istanze risolte 
    ritornare true '''
    # return True
    for p in PLANNERS:
        solved = RESULTS_DICT[p][d][instance][OTHERS]['aggregated_run']['Solved']
        n_run = len(RESULTS_DICT[p][d][instance][RUNS].keys())
        if int(solved) < (n_run / 2) + 1:
            return False
    return True


def avg_quantity(d, p, metric):
    ''' medio le mediane '''
    p_d_subdict = RESULTS_DICT[p][d]

    quantities = []

    for i_k, instance in p_d_subdict.iteritems():

        if solved_by_all(i_k, d):
            q = instance[OTHERS]['median_run'][metric]
            quantities.append(q)
        # print instance
        # for i_r, run in instance[OTHERS].iteritems():
        #     q = run[metric]
        #     quantities.append(q)

    # if metric == TIME:
    #     print quantities
    # print quantities
    # exit(1)
    # for instance_k in p_d_subdict.keys():
    #     print instance_k
    #     runs_k = p_d_subdict[instance_k][RUNS].keys()
    #     quantities.append(q)
    # print quantities
    # exit(1)
    # quantities = [
    #     p_d_subdict[instance_k][metric]
    #     for instance_k in p_d_subdict.keys()]

    quantities = clean_quantities(quantities)

    if len(quantities) != 0:
        ratio = sum(quantities) / len(quantities)
        return format_data_table(ratio, metric)
    else:
        return 'NA'


def clean_quantities(quantities):
    new_quantities = []
    for q in quantities:
        if 'NA' not in str(q):
            if q != ' ':
                new_quantities.append(float(q))

    # quantities = [q for q in quantities if 'NA' not in q and q != ' ']
    # quantities = [float(q) for q in quantities]
    return new_quantities


# def avg_total_quantity(p, metric):  # refactor
#     quantities = []

#     if metric == 'Quality':
#         print 'Funzione: {}'.format('avg_total_quantity')
#         print 'Pianificatore: {}'.format(p)
#         print 'Metric: {}'.format(metric)

#     for d in DOMAINS:
#         # if d == 'woodworking08':
#         #     continue
#         p_d_subdict = RESULTS_DICT[p][d]
#         for instance_k, instance in p_d_subdict.items():
#             quantities.append(instance[metric])

#     quantities = clean_quantities(quantities)
#     if len(quantities) != 0:
#         ratio = sum(quantities) / len(quantities)

#         if metric == 'Quality':
#             print 'Ratio: {}'.format(str(ratio))
#             print

#         return format_data_table(ratio, metric)
#     else:
#         return 'NA'


def avg_total_quantity(p, metric):  # refactor
    assert (MEDIAN_COMPARE is not None)

    quantities = []
    if MEDIAN_COMPARE is False:
        '''
        Cumulo e confronto tutto
        '''
        for d in DOMAINS:
            p_d_subdict = RESULTS_DICT[p][d]
            for instance_k, instance in p_d_subdict.items():
                ''' Controllo se l'istanza considerata e' risolta '''
                if instance['aggregated_run']['solved_instance']:
                    run_keys = get_just_key_run(instance.keys())

                    for run in run_keys:
                        q = instance[run][metric]
                        quantities.append(q)

    elif MEDIAN_COMPARE is True:
        '''
        Cumulo e confronto soltanto i dati mediani
        '''
        for d in DOMAINS:
            p_d_subdict = RESULTS_DICT[p][d]

            for instance_k, instance in p_d_subdict.items():
                ''' Controllo se l'istanza considerata e' risolta '''
                instance = instance[OTHERS]
                is_solved = instance['aggregated_run']['solved_instance']

                '''
                se e' risolta uso il dato mediano
                se non e' risolta e sto valutando la metric Time aggiungo 300.0

                aggiunta condizione per restringere la media al caso in cui tutti
                i pianificatori considerati hanno risolto la suddetta instanza
                    solved_by_all
                '''

                if metric == TIME:
                    if is_solved:
                        q = instance['median_run'][metric]
                    else:
                        q = 300.0
                    quantities.append(q)

                else:
                    if is_solved and solved_by_all(instance_k, d):
                        q = instance['median_run'][metric]
                        quantities.append(q)

                # if is_solved is True and solved_by_all(instance_k, d):
                #     q = instance['median_run'][metric]
                #     quantities.append(q)

                # elif is_solved is False and metric == TIME:  # inutile
                #     quantities.append(300.0)

    n_instances = len(quantities)
    if metric == TIME:
        assert n_instances == get_total_instances()

    ''' calcolo della media '''
    if len(quantities) != 0:
        # print p
        # print '{}'.format(metric)
        # print '{}'.format(len(quantities))
        # print '---------'
        ratio = sum(quantities) / n_instances
        return format_data_table(ratio, metric)

    else:
        return 'NA'

    # PRIMA VERSIONE
    # for d in DOMAINS:
    #     p_d_subdict = RESULTS_DICT[p][d]
    #     for instance_k, instance in p_d_subdict.items():
    #         q = instance['aggregated_run'][metric]
    #         quantities.append(q)
    # quantities = clean_quantities(quantities)
    # if len(quantities) != 0:
    #     ratio = sum(quantities) / len(quantities)
    #     return format_data_table(ratio, metric)
    # else:
    #     return 'NA'


def get_total_instances():
    total = 0
    for d in DOMAINS:
        total += INSTANCES_DICT[d]
    return total


def format_data_table(ratio, metric):
    if metric in [RECEIVED, EXPANDED_STATES, MESSAGES]:
        ratio = ratio / 1000.0
    return round(ratio, 2)


''' QUALITY '''


def get_total_avg_quality(p):
    return avg_total_quantity(p, QUALITY)


''' TIME '''


def get_total_avg_time(p):
    return avg_total_quantity(p, TIME)

'''IPC_T'''


def get_aggregated_ipc(d, p, x):
    instances = RESULTS_DICT[p][d].keys()
    # assert(len(instances) == 20)
    quantities = []
    for instance in instances:
        # instance_keys = RESULTS_DICT[p][d][instance].keys()
        run_keys = RESULTS_DICT[p][d][instance][RUNS]

        '''
        prendo il migliore
        todo: sarebbe meglio calcolarli prima...
        '''
        x_star = get_t_or_q_star(d, instance, x)

        '''
        Controllo se l'istanza e' risolta
        FARE IL REFACTOR
        '''
        aggr_data = RESULTS_DICT[p][d][instance][OTHERS]['aggregated_run']

        ''' 
        se l'istanza e' risolta calcolo IPC score altrimenti non 
        sommo niente (punteggio assegnato = 0)
        '''
        if aggr_data['solved_instance'] is True:

            ''' sommo tutto '''
            if MEDIAN_COMPARE is False:
                for run_k in run_keys:
                    x_p = get_t_or_q_p(p, d, instance, run_k, x)

                    ipc_to_add = calculate_ratio_ipc(x_star, x_p, x)
                    quantities.append(ipc_to_add)

            elif MEDIAN_COMPARE is True:

                # -1: prendi la mediana del campo x
                x_p = get_t_or_q_p(p, d, instance, -1, x)

                ipc_to_add = calculate_ratio_ipc(x_star, x_p, x)
                quantities.append(ipc_to_add)

    return round(sum(quantities), 2)


def get_just_key_run(instance_keys):
    return [x for x in instance_keys if is_int(x)]


def is_int(s):
    try:
        int(s)
        return True

    except ValueError:
        return False


def calculate_ratio_ipc(x_star, x_p, x):
    if x == T:  # TIME
        return calculate_time_ratio_ipc(x_star, x_p, IPC7)
    elif x == Q:  # QUALITY
        return calculate_quality_ratio_ipc(x_star, x_p)


def calculate_quality_ratio_ipc(x_star, x_p):
    assert (x_star <= x_p)
    assert (
        (x_star == float('inf') and x_p == 'NA') or (x_star != float('inf')))

    if x_star == float('inf'):
        return 0.0
    elif x_p == 'NA':
        return 0
    else:
        return x_star / x_p


def calculate_time_ratio_ipc(x_star, x_p, ipc_version):
    '''
    x_star: riferimento ''ottimo''
    x_p: riferimento corrente
    '''
    if ipc_version == IPC7:
        return calculate_time_ratio_ipc7(x_star, x_p)
    elif ipc_version == 'IPC6':
        print 'Da implementare!'


def calculate_quality_ratio_ipc6(x_star, x_p):
    ''' DISABILITATA '''
    # assert (x_star <= x_p)
    # assert (
    #     (x_star == float('inf') and x_p == 'NA') or (x_star != float('inf')))

    # if x_star == float('inf'):
    #     return 0.0
    # elif x_p == 'NA':
    #     return 0
    # else:
    #     return x_star / x_p
    pass


def calculate_time_ratio_ipc7(x_star, x_p):
    '''
    x_star: riferimento ''ottimo''
    x_p: riferimento corrente
    '''

    assert (x_star <= x_p)
    assert (
        (x_star == float('inf') and x_p == 'NA') or (x_star != float('inf')))

    # nessun pianificatore ha risolto l'istanza
    if x_star == float('inf'):
        return 0.0

    else:
        # non risolto, assegno 300 secondi (tempo massimo assegnato)
        if x_p == 'NA':
            x_p = 300.0

        # tempo di risoluzione inferiore a 1 secondo
        elif x_p < 1:
            return 1.0

        ratio = float(x_p) / float(x_star)

        return (1.0 / (1.0 + log10(ratio)))


''' SOLVED PROBLEMS '''


def get_aggregated_solved_problems(d, p):

    solved_problems = 0
    d_p_results = RESULTS_DICT[p][d]
    for instance_name, instance in d_p_results.items():
        aggregated_data = \
            d_p_results[instance_name][OTHERS]['aggregated_run']
        instance_keys = d_p_results[instance_name][RUNS]
        num_of_run = len(instance_keys)

        '''
        Versione piu' versatile, in cui un problema si considera
        risolto in funzione del numero di run per il quale e' stato
        eseguito per un certo planner p
        '''
        if float(aggregated_data['Solved']) >= float((num_of_run / 2) + 1):
            aggregated_data['solved_instance'] = True
            solved_problems += 1
        else:
            aggregated_data['solved_instance'] = False

        '''
        Versione semplice in cui ho un numero di run uguale
        per tutti i planner
        '''
        # if float(aggregated_data['Solved']) >= float(SOLVED_RUN):
        #     solved_problems += 1
    return solved_problems


def get_total_p_solved_problems(p):
    solved_problems_per_d = \
        [TABLE_DICT[d][p][METRICS][SOLVED_PROBLEMS] for d in DOMAINS]
    return sum(solved_problems_per_d)


def get_t_or_q_p(p, d, instance, run, t_or_q):
    which_par = TIME if t_or_q == T else QUALITY

    # ESTRAZIONE DATO
    p_dict = RESULTS_DICT[p]
    pd_dict = p_dict[d]
    instance_dict = pd_dict[instance]
    if run != -1 and run >= 1:
        run_dict = instance_dict[run]

    elif run == -1:
        run_dict = instance_dict[OTHERS]['median_run']

    else:
        print 'Errore nella funzione get_t_or_q_p'
        print 'Numero di run errato'

    str_t_p = run_dict[which_par]
    return str_to_float(str_t_p)


def quantity_sum(q, to_add):
    try:
        q = q + float(to_add)
        return q
    except ValueError:
        return q


def get_p_d_coverage(p, d):
    return TABLE_DICT[d][p][METRICS][SOLVED_PROBLEMS]


def get_p_coverage(p):
    # total_coverage = 0
    # for d in DOMAINS:
    #     total_coverage += get_p_d_coverage(p, d)
    # return total_coverage
    return TOTAL_DICT[p][SOLVED_PROBLEMS]

# USELESS
# def is_solved(instance_runs):
#     '''
#     Criterio:
#     Un'istanza non e' risolta quando tutti gli
#     indici presenti nel file .csv sono settati come NA
#     (per Time e Quality)
#     '''
#     solved_instances = count_solved(instance_runs)
#     return solved_instances >= SOLVED_RUN


# USELESS
# def is_solved_single_instance(instance):
#     '''
#     Restituisco che un'istanza non e' risolta quando tutti gli
#     indici presenti nel file .csv sono settati come NA
#     '''
#     if 'NA' in instance[TIME] and 'NA' in instance['Quality']:
#         return False
#     else:

#         return True


# USELESS
# def count_solved(instance_runs):
#     solved_instances = 0
#     for indx, instance in instance_runs.iteritems():
#         if is_solved_single_instance(instance):
#             solved_instances += 1
#     return solved_instances


def init_results_dict():
    ''' initialize results dict '''
    for p in PLANNERS:
        RESULTS_DICT[p] = {}
        for d in DOMAINS:
            RESULTS_DICT[p][d] = {}


def get_instances_dict():
    global INSTANCES_DICT
    for p in PLANNERS:
        INSTANCES_DICT[p] = {}
        path_p = join(PREPROCESS, p)
        p_results = read_file(path_p)

        rows = p_results.split('\n')

        fields = rows[0].split(',')
        indx_d = fields.index('Domain')
        indx_p = fields.index('problem')

        for r in rows:
            ''' skippo i campi '''
            if r == '' or r == rows[0]:
                continue
            s_row = r.split(',')
            d, instance = get_domain_from_row(s_row, indx_d, indx_p)

            update_instance_dict(p, d, instance)

    ''' controllo correttezza input '''
    # iterator = [k_dict for k, k_dict in INSTANCES_DICT.iteritems()]
    # if check_equal_list(iterator):
    INSTANCES_DICT = INSTANCES_DICT[p]
    for k, kv in INSTANCES_DICT.iteritems():
        INSTANCES_DICT[k] = len(set(kv))
    INSTANCES_DICT[TOTAL] = sum([kv for k, kv in INSTANCES_DICT.iteritems()])
    # else:
    #     print 'Controlla run'
    #     exit(1)


def check_equal_list(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == rest for rest in iterator)


def update_instance_dict(p, d, name):
    if d not in INSTANCES_DICT[p].keys():
        INSTANCES_DICT[p][d] = list()
        # INSTANCES_DICT[p][d] = 0
    if name not in INSTANCES_DICT[p][d]:
        INSTANCES_DICT[p][d].append(name)
    # INSTANCES_DICT[p][d] += 1


def get_domain_from_row(s_row, indx_d, indx_p):
    return clean_str(s_row[indx_d]), clean_str(s_row[indx_p])


def init_table_dict():
    ''' initialize table dict '''
    for d in DOMAINS:
        TABLE_DICT[d] = {}
        for p in PLANNERS:
            TABLE_DICT[d][p] = {}
            TABLE_DICT[d][p]['TOTAL'] = {}
            TABLE_DICT[d][p][METRICS] = {}


def init_total_dict():
    for p in PLANNERS:
        TOTAL_DICT[p] = {}


def init_p_star_dict():
    for d in DOMAINS:
        P_STAR_DICT[d] = {}
        P_STAR_DICT[d][Q] = {}
        P_STAR_DICT[d][T] = {}


def list_files(path):
    return [f for f in listdir(path) if isfile(join(path, f))]


def get_domains():
    global DOMAINS
    results_files = [f for f in list_files(RISULTATI) if '.py' not in f]


    DOMAINS = set([])
    for p_result in results_files:
        p_string = read_file(join(RISULTATI, p_result))
        p_domains = \
            set([x.split(',')[1] for x in p_string.split('\n') if x != ''])
        DOMAINS = DOMAINS | p_domains

    ''' Rimuovo dominio alza la media '''
    # DOMAINS.remove('woodworking08')
    # DOMAINS.remove('sokoban')

    ''' debug '''
    # DOMAINS = []
    # DOMAINS.append('MALogistics')
    # DOMAINS.append('MALogistics-hard')
    DOMAINS.remove('Domain')

    DOMAINS=[
        'blocksworld',
        'depot',
        'driverlog',
        'elevators08',
        'logistics00',
        'rovers',
        'satellites',
        'sokoban',
        'taxi',
        'wireless',
        'woodworking08',
        'zenotravel',
    ]




def get_results_indx(txt):
    global INSTANCE_FIELDS
    txt = txt.replace(' ', '')
    if INSTANCE_FIELDS == []:
        splitted_txt = txt.split('\n')
        first_row = splitted_txt[0]
        INSTANCE_FIELDS = (first_row.split(','))
    else:
        pass


# def populate_results_dict(p_results, p):
#     rows = split_results(p_results)

#     ''' populate dict '''
#     for r in rows:
#         instance_dict = {}
#         for indx_field, field in enumerate(INSTANCE_FIELDS, start=0):
#             instance_dict[field] = split_row(r)[indx_field]
#         assert(instance_dict['Planner'] == p)
#         ''' DEBUG senza woodworking '''
#         try:
#             add_instance_to_d(p, instance_dict['Domain'], instance_dict)
#         except:
#             pass
#     print instance_dict
#     print INSTANCE_FIELDS


def populate_results_dict(p_results, p):
    rows = split_results(p_results)

    ''' populate dict '''
    for r in rows:
        instance_dict = {}
        for indx_field, field in enumerate(INSTANCE_FIELDS, start=0):
            cleaned_string = clean_str(split_row(r)[indx_field])
            instance_dict[field] = cleaned_string

        assert(RUN in instance_dict.keys())

        run = instance_dict[RUN]

        assert(instance_dict['Planner'] == p)

        ''' try catch per rimozione domini '''
        try:
            add_instance_to_d(p, instance_dict['Domain'], run, instance_dict)
        except:
            pass


def clean_str(instr):
    output = instr.replace(' ', '').replace('\t', '').replace('\r', '')

    ''' temporaneamente i dati mancanti si assumono come NA '''
    if output == '' or output == '-1':
        output = 'NA'
    return output


# NO MULTI RUN
# def add_instance_to_d(p, d, instance_dict):
#     p_name = instance_dict['problem']
#     RESULTS_DICT[p][d][p_name] = {}
#     del instance_dict['Domain']
#     del instance_dict['Planner']
#     for field in instance_dict.keys():
#         RESULTS_DICT[p][d][p_name][field] = instance_dict[field]


# MULTIRUN
def add_instance_to_d(p, d, run, instance_dict):

    p_name = instance_dict['problem']
    '''
    se il problema non e' ancora stato visto lo aggiungo
    qui saranno presenti diversi run
    '''
    if p_name not in RESULTS_DICT[p][d].keys():
        RESULTS_DICT[p][d][p_name] = {}
        RESULTS_DICT[p][d][p_name][RUNS] = {}
        RESULTS_DICT[p][d][p_name][OTHERS] = {}

    # RESULTS_DICT[p][d][p_name][run] = {}
    RESULTS_DICT[p][d][p_name][RUNS][run] = {}

    ''' tolgo il superfluo '''
    del instance_dict['Domain']
    del instance_dict['Planner']
    del instance_dict[RUN]

    ''' preparo spazio per i run '''
    # for indx_run in xrange(1, EXPECTED_RUN + 1):
    #     RESULTS_DICT[p][d][p_name][str(indx_run)] = {}

    for field in instance_dict.keys():
        RESULTS_DICT[p][d][p_name][RUNS][run][field] = instance_dict[field]


def split_results(txt_results):
    rows = txt_results.split('\n')[1:]
    rows = [r for r in rows if r != '']
    return rows


def split_row(row):
    return row.split(',')


def read_file(path):
    with open(path, 'r') as fin:
        return fin.read()


if __name__ == "__main__":
    main()

import os

REPLICATE = False

''' inutile '''
# MEDIAN = False
# MEDIAN = not (REPLICATE)

MEDIAN_COMPARE = True
# RISULTATI = '/home/lpg/TestsScripts/RESULTS'
RISULTATI = 'RESULTS'
PREPROCESS = 'preprocessed'
OUTPUT_PATH = 'output_tables/{}'
CURRENT_FOLDER = os.getcwd()

'''
Expected Run : run attesi, serve solo se Replicate == TRUE
per replicare le istanze.
'''
# EXPECTED_RUN = 5
# SOLVED_RUN = EXPECTED_RUN / 2 + 1

''' keys and fields '''
SOLVED_PROBLEMS = 'solved_problems'
IPC_T, IPC_Q = 'ipc_t', 'ipc_q'
TIME = 'Time'
QUALITY = 'Quality'
MESSAGES = 'Msgs'
RECEIVED = 'Received'
EXPANDED_STATES = 'States'
RESTARTS_DONE = 'Restarts'
STATES_PRUNED = 'PrunedStates'
RESTART_SENT = 'RestartSend'
RESTART_RECEIVED = 'RestartReceived'
STD_DEVS = 'std_deviations'
T, Q = 'T', 'Q'
T_STAR, Q_STAR, P_STAR = 'T*', 'Q*', 'P*'
METRICS = 'METRICS'
RUN = 'run'
IPC7 = 'ipc7'
TOTAL = 'total'
RUNS = 'runs'
OTHERS = 'others'
DEV_T = 'dev_t'
DEV_Q = 'dev_q'
WEIGHTED_DEV_Q = 'wdev_q'
N_RUN = None


METRIC2DEV = {
    TIME: DEV_T,
    QUALITY: DEV_Q
}

DEV2METRIC = {
    DEV_T: TIME,
    DEV_Q: QUALITY,
    WEIGHTED_DEV_Q: QUALITY
}


''' dictionaries '''
INSTANCE_FIELDS = []
RESULTS_DICT = {}
TABLE_DICT = {}
TOTAL_DICT = {}
P_STAR_DICT = {}
INSTANCES_DICT = {}
SOLVED_BY_ALL = {}

'''formattazione'''
TABLE_FIELD = [
    SOLVED_PROBLEMS, TIME, QUALITY, MESSAGES, EXPANDED_STATES]

TABLE_FIELDS_LATEX = [tb for tb in TABLE_FIELD]
TABLE_FIELDS_LATEX.append(IPC_Q)
TABLE_FIELDS_LATEX.append(IPC_T)

AVG_FIELDS = [TIME, QUALITY, MESSAGES, RECEIVED, EXPANDED_STATES]
HLINE = '\t\\hline \n'

''' planners and domains '''
PLANNERS = []
DOMAINS = set([])


''' latek templates '''
TABLE_TEMPLATE = '''\\begin{table*}
\\tiny
\\begin{tabular}{#COL#}
#BODY#
\\end{tabular}
\\caption{#CAPTION#}
\\end{table*}'''

LATEX_TEMPLATE = '''\\documentclass{article}
\\addtolength{\oddsidemargin}{-0.8in}
\\usepackage{amsmath, amsthm, amssymb, amsfonts}
\\addtolength{\\oddsidemargin}{-0.8in}
\\newcommand\\mabfws{$\\sf MA$-$\\sf BFWS$}
\\newcommand\\blocksworld{$\\rm Blocksworld$}
\\newcommand\\depot{$\\rm Depot$}
\\newcommand\\driverlog{$\\rm DriverLog$}
\\newcommand\\elevators{$\\rm Elevators$} %'08
\\newcommand\\logistics{$\\rm Logistics$} %'00
\\newcommand\\rovers{$\\rm Rovers$}
\\newcommand\\satellites{$\\rm Satellites$}
\\newcommand\\sokoban{$\\rm Sokoban$}
\\newcommand\\taxi{$\\rm Taxi$}
\\newcommand\\wireless{$\\rm Wireless$}
\\newcommand\\woodworking{$\\rm Woodworking$} %'08
\\newcommand\\zenotravel{$\\rm Zenotravel$}
\\newcommand\\mabw{$\\rm MA$-$\\rm Blocksworld$}
\\newcommand\\mabwhard{$\\rm MA$-$\\rm Blocksworld$-$\\rm Hard$}
\\newcommand\\malog{$\\rm MA$-$\\rm Logistics$}
\\newcommand\\maloghard{$\\rm MA$-$\\rm Logistics$-$\\rm Hard$}
\\newcommand\\mabwshort{$\\rm MA$-$\\rm BW$}
\\newcommand\\mabwhardshort{$\\rm MA$-$\\rm BW$-$\\rm H$}
\\newcommand\\malogshort{$\\rm MA$-$\\rm Log$}
\\newcommand\\maloghardshort{$\\rm MA$-$\\rm Log$-$\\rm H$}
\\newcommand\\hffp{$h_{\\sf FF}^P$}

\\begin{document}

#BODY#

\\end{document}'''


'''PLANNING SYSTEM'''
DOUBLE_ROW_PLANNER = \
    '\\begin{tabular}[c]{@{}l@{}}#FIRST#\\\\ #SECOND#\\end{tabular}'

Dual_SIW_RA_DA = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'Dual SIW').replace('#SECOND#', 'RA DA')

Dual_C1_SIW_RA_DA = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'Dual C1 SIW').replace('#SECOND#', 'RA DA')

Dual_BFWS_N2_DA = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'Dual BFWS').replace('#SECOND#', 'N2 DA')

Dual_BFWS_N2 = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'Dual BFWS').replace('#SECOND#', 'N2')

Dual_C1_BFWS_N2 = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'Dual C1').replace('#SECOND#', 'BFWS N2')

MAPlan_FFplusDTG = DOUBLE_ROW_PLANNER.replace(
    '#FIRST#', 'MAPlan').replace('#SECOND#', 'FF+DTG')


'''VARIANT DICT'''
VARIANT_DICT = {
    'SOCS_20180227_local_reach_RA_DA_DUAL': 'SIW Do Alone SQ',
    'SOCS_20180227_local_reach_RA_DA_DUAL_C1': 'Dual SIW Do Alone E1',
    'planner3': MAPlan_FFplusDTG,  # 5 minuti
    'planner3_30': 'MAPlan FF+DTG 30min',
    'SOCS_20180227_local_reach_RA': 'SIW',
    'SOCS_20180227_local_reach_RA_DA': 'SIW Do Alone',
    'SOCS_20180227_local_reach_RA_C1': 'SIW E1',
    'SOCS_20180227_local_reach_RA_DA_C1': 'SIW Do Alone E1',
    'LPG_MA_20171228_share_no_agent_actions_msggraph_h0': 'Bfs Hff',
    'LPG_MA_SIW_20171201_local_reach_RA_HMsender_img': 'SIW RA',
    'LPG_MA_SIW_20171207_local_reach_RA_DA3_HMsender_unovelty_img': \
    Dual_SIW_RA_DA,
    'LPG_MA_C1_20171228_local_reach_RA2_DA4_HMsender_unovelty_img': \
    Dual_C1_SIW_RA_DA,
    'LPG_MA_BFWS_20180118_A2_F0_img': 'BFWS N2',
    'LPG_MA_BFWS_20180118_A2_F0_DA3_HMsender_unovelty_img': Dual_BFWS_N2_DA,
    'LPG_MA_BFWS_20180118_A2_F0_unovelty_img': Dual_BFWS_N2,
    'LPG_MA_C1_20180123_BFWS_A2_F0_unovelty_img': Dual_C1_BFWS_N2,
    'CMAP-t': 'CMAP-t',
    'MAPlan FF+DTG': MAPlan_FFplusDTG,
    'SOCS_20180227_local_reach_RA_DUAL': 'Local Reach RA DUAL',
    'SOCS_20180227_local_reach_RA_DUAL_C1': 'Local Reach RA DUAL C1',
    'LPG_MA_BFWS_20180918_A2_F3_unovelty_FN_img_exchpred': 'A2F3',
    'LPG_MA_BFWS_20180918_A2_F3_unovelty_FN_img_exchpred_FN': 'A2F3FN',
    'LPG_MA_BFWS_20180918_A2_F3_unovelty_FN_img_exchpred_UpdateNovelty': \
    'A2F3UN',
    'LPG_MA_BFWS_20180919_A2_F10_unovelty_FN_img_exchpred': 'A2F10',
    'LPG_MA_BFWS_20180919_A2_F11_unovelty_FN_img_exchpred': 'A2F11',
    'LPG_MA_BFWS_20180919_A2_F0_unovelty_FN_img_exchpred': 'A2F0',
    'LPG_MA_C1_20180919_A2_F10_unovelty_FN_img_exchpred': 'A2F10',
    'LPG_MA_BFWS_20180919_A2_F3_unovelty_FN_img_exchpred': 'A2F3',
    'LPG_MA_BFWS_20180919_A2_F20_unovelty_FN_img_exchpred': 'A2F20',
    'LPG_MA_BFWS_20180919_A2_F1_unovelty_FN_img_exchpred': 'A2F1',
    'LPG_MA_20181006_share_no_agent_actions_h1': 'share no',
    'LPG_MA_BFWS_20180919_A1_F10_FN_img_exchpred': 'A1 F10',
    'LPG_MA_BFWS_20180919_A2_F10_FN_img_exchpred': 'A2F10',
    # TASK1
    # 'LPG_MA_20181025_NICO_A2_F0_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_0$\\\\ E0 no prun\\end{tabular}',
    # TASK2
    'LPG_MA_20181025_NICO_A2_F0_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}} $f_0$ $a_2$ \\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F1_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_1$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F3_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_3$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F10_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_{10}$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F20_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_{20}$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F31_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_{31}$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181025_NICO_A2_F40_unovelty_FN_img_exchpred_NEW': '\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\ E0 no prun\\end{tabular}',
    'LPG_MA_20181027_share_no_agent_actions_h1': '\\begin{tabular}[c]{@{}c@{}} BFS \\\\ E0 no prun\\end{tabular}',
    #
    'LPG_MA_20181025_NICO_A1_F0_FN_img_exchpred_NEW': '$f_0$ $a_1$',
    'LPG_MA_20181025_NICO_A2_F0_FN_img_exchpred_NEW': '$f_0$ $a_2$',
    #
    'LPG_MA_C1_20181027_A2_F0_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_0$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181027_A2_F1_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_1$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181027_A2_F3_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_3$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181027_A2_F20_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_{20}$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181027_A2_F31_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_{31}$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181027_A2_F40_unovelty_FN_img_exchpred': '\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\ E1 no prun\\end{tabular}',
    'LPG_MA_C1_20181115_A2_F40_unovelty_FN_img_exchpred_NoAdd': '\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\ E1 no prun\\end{tabular}',
    #
    'E1_20181111_BFS_share_no_agent_actions_hff':'\\begin{tabular}[c]{@{}c@{}} $BFS \; h_{ff}$ \\\\ ** E1\\end{tabular}',
    'E1_20181027_BFS_share_no_agent_actions_h1':'\\begin{tabular}[c]{@{}c@{}}$BFS \; h^p_{ff}$\\\\ E1\\end{tabular}',
    'E1_20181027_SIW_do_alone_second_queue':'\\begin{tabular}[c]{@{}c@{}}SIW\\\\ E1\\end{tabular}',
    #'E1_20181027_BFWS_A1_F6_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$<w_{depth},h_{ff}> $\\\\ E1 PR\\end{tabular}',
    #'E1_20181027_BFWS_A2_F6_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$<w_{depth},h_{ff}> $\\\\ E1 PR\\end{tabular}',
    'E1_20181027_BFWS_A1_F6_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_6, N1 $\\\\ E1 PR\\end{tabular}',
    'E1_20181027_BFWS_A2_F6_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_6 $\\\\ E1 PR\\end{tabular}',
    'E1_20181027_BFWS_A2_F6_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_6$\\\\ E1 NP\\end{tabular}',
    'E1_20181111_BFWS_A1_F0_unovelty_FN_img_exchpred_hff':'\\begin{tabular}[c]{@{}c@{}}$f_0$, N1\\\\** E1 NP\\end{tabular}',
    'E1_20181111_BFWS_A2_F0_unovelty_FN_img_exchpred_hff':'\\begin{tabular}[c]{@{}c@{}}$f_0$\\\\** E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F0_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_0$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F1_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_1$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F2_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_2$\\\\ E1 NP\\end{tabular}',

    'E1_20181111_BFWS_A2_F3_unovelty_FN_img_exchpred_hff':'\\begin{tabular}[c]{@{}c@{}}$f_3$\\\\** E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F3_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_3$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F10_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_{10}$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F11_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_{11}$\\\\ E1 NP\\end{tabular}',

    'E1_20181111_BFWS_A2_F20_unovelty_FN_img_exchpred_hff':'\\begin{tabular}[c]{@{}c@{}}$f_{20}$\\\\**  E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F20_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f^p_{20}$\\\\ ** E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F30_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_{30}$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F31_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_{31}$\\\\ ** E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F40_unovelty_FN_img_exchpred':'\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\ E1 NP\\end{tabular}',
    'E1_20181027_BFWS_A2_F40_unovelty_FN_img_exchpred_NoAdd':'\\begin{tabular}[c]{@{}c@{}}$f_{40}NoAdd$\\\\** E1 NP\\end{tabular}',
    #
    'E1_20181111_BFS_share_no_agent_actions_hff_30min':'\\begin{tabular}[c]{@{}c@{}} $BFS \; h_{ff}$ \\\\ E1\\end{tabular}',
    'E1_20181027_SIW_do_alone_second_queue_30min':'\\begin{tabular}[c]{@{}c@{}} SIW \\\\ E1\\end{tabular}',
    'E1_20181027_BFWS_A2_F40_unovelty_FN_img_exchpred_30min':'\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\E1 NP\\end{tabular}',
    'E0_20181027_BFWS_A2_F40_unovelty_FN_img_exchpred_30min':'\\begin{tabular}[c]{@{}c@{}}$f_{40}$\\\\E0 NP\\end{tabular}',
    'E1_20181112_BFWS_Single_A1_F4_FN_img_exchpred': '$\\langle w,\#d \\rangle=1$',
    'E1_20181112_BFWS_Single_A1_F7_FN_img_exchpred': '$\\langle w_{\leq \#d},h^p_{ff} \\rangle=1 $',
    'E1_20181112_BFWS_Single_A1_F8_FN_img_exchpred': '$\\langle w_{\leq \#d},\#d \\rangle=1$',
    'E1_20181112_BFWS_Single_A2_F4_FN_img_exchpred': '$\\langle w,\#d \\rangle=2$',
    'E1_20181112_BFWS_Single_A2_F7_FN_img_exchpred': '$\\langle w_{\leq \#d},h^p_{ff} \\rangle=2$',
    'E1_20181112_BFWS_Single_A2_F8_FN_img_exchpred': '$\\langle w_{\leq \#d},\#d \\rangle=2$',
    'E1_20181112_BFWS_Single_A2_F4_FN_img_exchpred_nopruning': '$\\langle w,\#d \\rangle \ge 2$',
    'E1_20181112_BFWS_Single_A2_F7_FN_img_exchpred_nopruning': '$\\langle w_{\leq \#d},h^p_{ff} \\rangle \ge 2$',
    'E1_20181112_BFWS_Single_A2_F8_FN_img_exchpred_nopruning': '$\\langle w_{\leq \#d},\#d \\rangle \ge 2$'
}


TABLE_CAPTION = '''Number of problems solved by \\mabfws\\ with seven different heuristics.  The best performance are indicated in bold.
\\%$h_1=\\langle w_{h_{ff}},h_{ff} \\rangle$; 
\\%$h_2=\\langle w_{\\#g,h_{ff}},\\#g,h_{ff} \\rangle$; 
\\%$h_{3}=\\langle w_{\\#g_u,\\#g,h_{ff}},\#g_u,\\#g,h_{ff} \\rangle$;
\\%$h_{4}=\\langle w_{\\#g_u,\\#g,h^p_{ff}},\\#g_u,\\#g,h^p_{ff} \\rangle$;
\\%$h_{5}=\\langle w_{\\#g_u,\\#g,\\#r},\\#g_u,\\#g,\\#r \\rangle$; 
\\%$h_{6}=\\langle w_{\\#g,\\#r_{I}},\#g,\\#r_{I} \\rangle$;''' 


''' COVERAGE PLOT '''
GRANULARITY = 5  # in millisec
SOLVED_BY_TIME = {}
GNUPLOT_TEMPLATE = 'template_gplot_coverage'
COVERAGE_DOCUMENT = 'plot_coverage.tex'

CODMAP_DOMAINS = [
    'blocksworld',
    'depot',
    'driverlog',
    'elevators08',
    'logistics00',
    'rovers',
    'satellites',
    'sokoban',
    'taxi',
    'wireless',
    'woodworking08',
    'zenotravel']

LATEX_DOMAIN = {
    'blocksworld': '\\blocksworld',
    'depot': '\\depot',
    'driverlog': '\\driverlog',
    'elevators08': '\\elevators',
    'logistics00': '\\logistics',
    'rovers': '\\rovers',
    'satellites': '\\satellites',
    'sokoban': '\\sokoban',
    'taxi': '\\taxi',
    'wireless': '\\wireless',
    'woodworking08': '\\woodworking',
    'zenotravel': '\\zenotravel',
    'MABlocksWorld': '\\mabwshort',
    'MALogistics': '\\mabwhardshort',
    'MABlocksWorld-hard': '\\malogshort',
    'MALogistics-hard': '\\maloghardshort'}

PLOT_DICT = {
    'E1_20181111_BFS_share_no_agent_actions_hff': '$h_{\\\\sf FF}$',
    'E1_20181111_BFWS_A2_F0_unovelty_FN_img_exchpred_hff': '$f_1$',
    'E1_20181111_BFWS_A2_F3_unovelty_FN_img_exchpred_hff': '$f_2$',
    'E1_20181111_BFWS_A2_F20_unovelty_FN_img_exchpred_hff': '$f_3$',
    'E1_20181027_BFWS_A2_F20_unovelty_FN_img_exchpred': '$f_4$',
    'E1_20181027_BFWS_A2_F30_unovelty_FN_img_exchpred': '$f_5$',
    'E1_20181027_BFWS_A2_F40_unovelty_FN_img_exchpred': '$f_6$',
}
